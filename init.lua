--[[

studs – Minetest mod to add LEGO-like studs to nodes
Copyright © 2022  Nils Dagsson Moskopp (erlehmann)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Dieses Programm hat das Ziel, die Medienkompetenz der Leser zu
steigern. Gelegentlich packe ich sogar einen handfesten Buffer
Overflow oder eine Format String Vulnerability zwischen die anderen
Codezeilen und schreibe das auch nicht dran.

]]--

local node_box_carpet = {
	type = "fixed",
	fixed = {
		{-8/16, -8/16, -8/16, 8/16, -5/16, 8/16},
	},
}

local node_box_normal = {
	type = "fixed",
	fixed = {
		{ -8/16, -8/16, -8/16,  8/16,  8/16,  8/16 },
		{ -6/16,  8/16, -6/16, -2/16, 10/16, -2/16 },
		{  6/16,  8/16,  6/16,  2/16, 10/16,  2/16 },
		{  6/16,  8/16, -6/16,  2/16, 10/16, -2/16 },
		{ -6/16,  8/16,  6/16, -2/16, 10/16,  2/16 },
	},
}

local node_box_slab = {
	type = "fixed",
	fixed = {
		{ -8/16, -8/16, -8/16,  8/16,  0/16,  8/16 },
		{ -6/16,  0/16, -6/16, -2/16,  2/16, -2/16 },
		{  6/16,  0/16,  6/16,  2/16,  2/16,  2/16 },
		{  6/16,  0/16, -6/16,  2/16,  2/16, -2/16 },
		{ -6/16,  0/16,  6/16, -2/16,  2/16,  2/16 },
	},
}

local node_box_stair = {
	type = "fixed",
	fixed = {
		{ -8/16, -8/16, -8/16,  8/16,  0/16,  8/16 },
		{ -8/16,  0/16,  0/16,  8/16,  8/16,  8/16 },
		{ -6/16,  0/16, -6/16, -2/16,  2/16, -2/16 },
		{  6/16,  8/16,  6/16,  2/16, 10/16,  2/16 },
		{  6/16,  0/16, -6/16,  2/16,  2/16, -2/16 },
		{ -6/16,  8/16,  6/16, -2/16, 10/16,  2/16 },
	}
}

local node_box_stair_outer = {
	type = "fixed",
	fixed = {
		{ -8/16, -8/16, -8/16,  8/16,  0/16,  8/16 },
		{ -8/16,  0/16,  0/16,  0/16,  8/16,  8/16 },
		{ -6/16,  0/16, -6/16, -2/16,  2/16, -2/16 },
		{  6/16,  0/16,  6/16,  2/16,  2/16,  2/16 },
		{  6/16,  0/16, -6/16,  2/16,  2/16, -2/16 },
		{ -6/16,  8/16,  6/16, -2/16, 10/16,  2/16 },
	}
}

local node_box_stair_inner = {
	type = "fixed",
	fixed = {
		{ -8/16, -8/16, -8/16,  8/16,  0/16,  8/16 },
		{ -8/16,  0/16,  0/16,  8/16,  8/16,  8/16 },
		{ -8/16,  0/16, -8/16,  0/16,  8/16,  0/16 },
		{ -6/16,  8/16, -6/16, -2/16, 10/16, -2/16 },
		{  6/16,  8/16,  6/16,  2/16, 10/16,  2/16 },
		{  6/16,  0/16, -6/16,  2/16,  2/16, -2/16 },
		{ -6/16,  8/16,  6/16, -2/16, 10/16,  2/16 },
	}
}

local get_node_box = function(node_name, node_def)
	local node_box
	if node_def.is_ground_content then
		-- do not ccange common nodes to not ruin fps
	elseif "normal" == node_def.drawtype then
		node_box = node_box_normal
	elseif (
		-- both nodebox and mesh stairs exist
		"nodebox" == node_def.drawtype or
		"mesh" == node_def.drawtype
	) then
		if string.match(node_name, "_carpet") then
			node_box = node_box_carpet
		elseif string.match(node_name, ":slab_") then
			node_box = node_box_slab
		elseif string.match(node_name, ":stair_") then
			if string.match(node_name, "_inner") then
				node_box = node_box_stair_inner
			elseif string.match(node_name, "_outer") then
				node_box = node_box_stair_outer
			else
				node_box = node_box_stair
			end
		end
	end
	return node_box
end

local add_studs = function()
	for node_name, node_def in pairs(minetest.registered_nodes) do
		local node_box = get_node_box(node_name, node_def)
		if nil ~= node_box then
			minetest.override_item(
				node_name,
				{
					drawtype = "nodebox",
					node_box = node_box,
					selection_box = node_box,
				}
			)
		end
	end
end

minetest.register_on_mods_loaded(add_studs)
